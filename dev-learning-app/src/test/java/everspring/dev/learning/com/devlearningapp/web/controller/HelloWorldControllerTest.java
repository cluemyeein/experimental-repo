package everspring.dev.learning.com.devlearningapp.web.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class HelloWorldControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private static final String baseUrl = "/";

    @Test
    public void get_Hello_World_Should_Return_Ok() throws Exception {
        String retMsg = "Hello World!";
        mockMvc.perform(MockMvcRequestBuilders.get(baseUrl).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().string(equalTo(retMsg)));
    }

    /**
     * This tests an invalid url.
     *
     * @throws Exception
     */
    @Test
    public void get_Hello_World_Should_Return_Not_Found() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(baseUrl+"not-there").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
    }

}