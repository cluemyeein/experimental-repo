package everspring.dev.learning.com.devlearningapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevLearningAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevLearningAppApplication.class, args);
	}

}
